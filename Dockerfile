# stage-0 for build
FROM maven:3-jdk-8
WORKDIR /tmp
## cache dependencies separately
## only runs if pom.xml changes
COPY pom.xml .
RUN mvn verify clean --fail-never
## build from source
COPY src/ ./src/
RUN mvn package

# stage-1 for execution
FROM openjdk:8-jre-slim
WORKDIR /app
COPY --from=0 /tmp/target/backend-*.jar /app/AphasiaHelpBackend.jar
ENTRYPOINT ["java","-jar","AphasiaHelpBackend.jar"]
EXPOSE 8080
ENV DB_HOST="localhost" \
    DB_PORT="5432" \
    GOOGLE_CLIENT_ID="" \
    GOOGLE_CLIENT_SECRET="" \                                                
    ORCID_CLIENT_ID="" \                                           
    ORCID_CLIENT_SECRET=""