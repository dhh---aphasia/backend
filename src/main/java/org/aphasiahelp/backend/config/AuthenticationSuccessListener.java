package org.aphasiahelp.backend.config;

import org.aphasiahelp.backend.model.keys.OidProvider;
import org.aphasiahelp.backend.service.AphasiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.oauth2.client.authentication.OAuth2LoginAuthenticationToken;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationSuccessListener implements ApplicationListener<AuthenticationSuccessEvent> {
	@Autowired AphasiaService aphasiaService;

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
    	OAuth2LoginAuthenticationToken token = (OAuth2LoginAuthenticationToken)event.getAuthentication();
    	OidProvider oidProvider = OidProvider.fromString(token.getClientRegistration().getRegistrationId());
    	String oid = token.getName();
    	aphasiaService.getOrCreateBaseUser(oidProvider, oid);
    }

}