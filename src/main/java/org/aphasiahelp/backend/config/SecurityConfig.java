package org.aphasiahelp.backend.config;

import org.aphasiahelp.backend.service.AphasiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired AphasiaService aphasiaService;
	
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
        	.antMatchers("/api/**/*", "/swagger-ui.html").authenticated()
        	.anyRequest().permitAll()
	        .and().oauth2Login()
	        .and().logout().logoutSuccessUrl("/index.html")
	        .and().csrf().disable();
    }

}