package org.aphasiahelp.backend.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="aphasia_type")
public class AphasiaType extends BaseEntity {
	
	@NotEmpty private String type;

	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}

}