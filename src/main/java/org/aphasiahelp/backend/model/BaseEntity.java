package org.aphasiahelp.backend.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@MappedSuperclass
public abstract class BaseEntity {
	
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(updatable = false, nullable = false)
	@Id private UUID id;

	public UUID getId() {
		return id;
	}
	
	@CreationTimestamp private LocalDateTime createDateTime;

	public LocalDateTime getCreateDateTime() {
		return createDateTime;
	}
	
	@UpdateTimestamp private LocalDateTime updateDateTime;

	public LocalDateTime getUpdateDateTime() {
		return updateDateTime;
	}
	
}