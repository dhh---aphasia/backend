package org.aphasiahelp.backend.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.aphasiahelp.backend.model.keys.OidProvider;

@MappedSuperclass
public abstract class BaseUser extends BaseEntity {

	public BaseUser() {};
	
	public BaseUser(OidProvider oidProvider, String oid) {
		setOidProvider(oidProvider);
		setOid(oid);
	}
	
	@Column(updatable = false, nullable = false)
	boolean admin = false;
	
	public boolean isAdmin() {
		return admin;
	}
	
	@Enumerated(EnumType.STRING)
	@NotNull private OidProvider oidProvider;
	
	public OidProvider getOidProvider() {return oidProvider;
	}
	public void setOidProvider(OidProvider oidProvider) {
		this.oidProvider = oidProvider;
		scientist = oidProvider.isScientist();
	}
	
	@NotEmpty 
	@NotNull private String oid;
	
	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}
	
	@Column	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Transient
	private boolean scientist = false;
	
	public boolean isScientist() {
		return getOidProvider() != null && getOidProvider().isScientist();
	}
	
}