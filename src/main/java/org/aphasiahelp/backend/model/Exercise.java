package org.aphasiahelp.backend.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.aphasiahelp.backend.model.keys.Language;

@Entity
@Table(name = "exercises")
public class Exercise extends BaseEntity {

	public Exercise() {}
	
	public Exercise(Language language, String text, Double difficulty) { 
		setLanguage(language);
		setText(text);
		setDifficulty(difficulty);
	}
	
	
	@NotNull private Language language;
	
	public Language getLanguage() {
		return language;
	}
	
	public void setLanguage(Language language) {
		this.language = language;
	}
	
	@NotEmpty private String text;

	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	@NotNull private Double difficulty;
	
	public Double getDifficulty() {
		return difficulty;
	}
	
	public void setDifficulty(Double difficulty) {
		this.difficulty = difficulty;
	}
	
}