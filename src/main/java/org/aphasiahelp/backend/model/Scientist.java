package org.aphasiahelp.backend.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.aphasiahelp.backend.model.keys.OidProvider;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="scientists",uniqueConstraints=@UniqueConstraint(name="uniqueScientists",columnNames={"oidProvider", "oid"}))
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Scientist extends BaseUser {
	
	public Scientist() {
		super();
	}
	
	public Scientist(OidProvider oidProvider, String oid) {
		super(oidProvider, oid);
	}

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "users_scientists" 
		, joinColumns = @JoinColumn(name = "scientist_id")
		, foreignKey = @ForeignKey(name = "scientists2users")
		, inverseJoinColumns = @JoinColumn(name = "user_id"))
	List<User> users;
	
	public List<User> getUsers() {
		return users;
	}
	
	public void setUsers(List<User> users) {
		this.users = users;
	}
	
}