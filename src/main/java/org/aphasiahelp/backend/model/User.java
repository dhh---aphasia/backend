package org.aphasiahelp.backend.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.aphasiahelp.backend.model.keys.OidProvider;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="users", uniqueConstraints=@UniqueConstraint(name="uniqueUsers", columnNames={"oidProvider", "oid"}))
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class User extends BaseUser {
	
	public User() {
		super();
	}
	
	public User(OidProvider oidProvider, String oid) {
		super(oidProvider, oid);
	}
	
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(	name = "users_scientists" 
		, joinColumns = @JoinColumn(name = "user_id")
		, foreignKey = @ForeignKey(name = "users2scientists")
		, inverseJoinColumns = @JoinColumn(name = "scientist_id"))
	List<Scientist> scientists;

	public List<Scientist> getScientists() {
		return scientists;
	}
	
	public void setScientists(List<Scientist> scientists) {
		this.scientists = scientists;
	}

	public void addScientist(Scientist scientist) {
		if(this.scientists == null)
			scientists = new ArrayList<Scientist>();
		scientists.add(scientist);
	}
	
	public void removeScientist(Scientist scientist) {
		if(scientists!=null)
			scientists.remove(scientist);
	}
	
}