package org.aphasiahelp.backend.model.keys;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Language {
	en,
	nl;
	
	@JsonCreator
    public static Language fromString(String name) {
        return valueOf(name);
    }
	
    @JsonValue
    public String getName() {
        return name();
    }
	
}
