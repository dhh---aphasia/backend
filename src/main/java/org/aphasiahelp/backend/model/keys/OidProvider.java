package org.aphasiahelp.backend.model.keys;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum OidProvider {
	google(false), 
	orcid(true);
	
	private boolean isScientist;
	
	private OidProvider(boolean isScientist) {
		this.isScientist = isScientist; 
	}
	
	public boolean isScientist() {
		return isScientist;
	}
	
	@JsonCreator
    public static OidProvider fromString(String name) {
        return OidProvider.valueOf(name);
    }
	
    @JsonValue
    public String getName() {
        return name();
    }
}
