package org.aphasiahelp.backend.repository;

import java.util.UUID;

import org.aphasiahelp.backend.model.ContactForm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactFormRepository extends JpaRepository<ContactForm, UUID> {}
