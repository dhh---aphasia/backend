package org.aphasiahelp.backend.repository;

import java.util.UUID;

import org.aphasiahelp.backend.model.Exercise;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ExerciseRepository extends PagingAndSortingRepository<Exercise, UUID> { }