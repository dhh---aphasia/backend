package org.aphasiahelp.backend.repository;

import java.util.UUID;

import org.aphasiahelp.backend.model.Scientist;
import org.aphasiahelp.backend.model.keys.OidProvider;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScientistRepository extends PagingAndSortingRepository<Scientist, UUID> { 
	
	public Scientist findByOidProviderAndOid(OidProvider oidProvider, String oid);
	
    public boolean existsByOidProviderAndOid(OidProvider oidProvider, String oid);

}