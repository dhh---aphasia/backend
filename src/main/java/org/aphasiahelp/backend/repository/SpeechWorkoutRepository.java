package org.aphasiahelp.backend.repository;

import java.util.UUID;

import org.aphasiahelp.backend.model.SpeechWorkout;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SpeechWorkoutRepository extends PagingAndSortingRepository<SpeechWorkout, UUID> {

}
