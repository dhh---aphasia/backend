package org.aphasiahelp.backend.repository;

import java.util.UUID;

import org.aphasiahelp.backend.model.User;
import org.aphasiahelp.backend.model.keys.OidProvider;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, UUID> { 
	
	public User findUserByOidProviderAndOid(OidProvider oidProvider, String oid);
	
    public boolean existsByOidProviderAndOid(OidProvider oidProvider, String oid);

}