package org.aphasiahelp.backend.service;

import java.io.IOException;
import java.util.UUID;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.aphasiahelp.backend.model.BaseUser;
import org.aphasiahelp.backend.model.ContactForm;
import org.aphasiahelp.backend.model.Exercise;
import org.aphasiahelp.backend.model.Scientist;
import org.aphasiahelp.backend.model.SpeechWorkout;
import org.aphasiahelp.backend.model.User;
import org.aphasiahelp.backend.model.keys.Language;
import org.aphasiahelp.backend.model.keys.OidProvider;
import org.aphasiahelp.backend.repository.ContactFormRepository;
import org.aphasiahelp.backend.repository.ExerciseRepository;
import org.aphasiahelp.backend.repository.ScientistRepository;
import org.aphasiahelp.backend.repository.SpeechWorkoutRepository;
import org.aphasiahelp.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/v1")	
public class AphasiaService {
	
	@Autowired UserRepository userRepository;
	@Autowired ExerciseRepository exerciseRepository;
	@Autowired ScientistRepository scientistRepository;
	@Autowired SpeechWorkoutRepository speechWorkoutRepository;
	@Autowired ContactFormRepository contactFormRepository;
	
	@RequestMapping(value = "/addExcercise", method = RequestMethod.PUT, produces = "application/json")
	private Exercise addExcercise(@RequestParam String text, @RequestParam Double difficulty, @RequestParam Language language) {
		Exercise excercise = new Exercise();
		excercise.setText(text);
		excercise.setDifficulty(difficulty);
		excercise.setLanguage(language);
		return exerciseRepository.save(excercise);
	}
	
	@RequestMapping(value = "/getAllExcercises", method = RequestMethod.GET, produces = "application/json")
	public Iterable<Exercise> getAllExcercises(
			@RequestParam(required = false, defaultValue = "1") @Min(value = 1) Integer page
			, @RequestParam(required = false, defaultValue = "20") @Min(value = 1) @Max(value = 100) Integer pageSize) {
		int p = page != null ? page.intValue()-1:0;
		int ps = pageSize != null ? pageSize.intValue():20;
		return exerciseRepository.findAll(PageRequest.of(p, ps)).getContent();
	}
	
	@RequestMapping(value = "/getAllScientists", method = RequestMethod.GET, produces = "application/json")
	public Iterable<Scientist> getAllScientists(
			@RequestParam(required = false, defaultValue = "1") @Min(value = 1) Integer page
			, @RequestParam(required = false, defaultValue = "20") @Min(value = 1) @Max(value = 100) Integer pageSize) {
		int p = page != null ? page.intValue()-1:0;
		int ps = pageSize != null ? pageSize.intValue():20;
		return scientistRepository.findAll(PageRequest.of(p, ps)).getContent();
	}
	
	@RequestMapping(value = "/getAllUsers", method = RequestMethod.GET, produces = "application/json")
	public Iterable<User> getAllUsers(
			@RequestParam(required = false, defaultValue = "1") @Min(value = 1) Integer page
			, @RequestParam(required = false, defaultValue = "20") @Min(value = 1) @Max(value = 100) Integer pageSize) {
		int p = page != null ? page.intValue()-1:0;
		int ps = pageSize != null ? pageSize.intValue():20;
		return userRepository.findAll(PageRequest.of(p, ps)).getContent();
	}
	
	@RequestMapping(value="/getCurrentUser", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation("Logs in a User via Oauth. ORCID oauth id's logs the User in as scientist. If the user does not exist, it is being generated")
	public BaseUser getCurrentUser() {
		return getOrCreateBaseUser();
	}
	
	@PostMapping(value="/contactForm", produces = "application/json")
	public ContactForm saveContactForm(@RequestParam String email, @RequestParam String text) {
		ContactForm contactForm = new ContactForm();
		contactForm.setEmail(email);
		contactForm.setText(text);
		return contactFormRepository.save(contactForm);
	}
	
	public BaseUser getOrCreateBaseUser() {
		OAuth2AuthenticationToken authentication = (OAuth2AuthenticationToken)(SecurityContextHolder.getContext().getAuthentication());
		OidProvider oidProvider = OidProvider.fromString(authentication.getAuthorizedClientRegistrationId());
		String oid = authentication.getName();
		
		return getOrCreateBaseUser(oidProvider, oid);
	}
	
	@PostMapping(value="/saveSpeechWorkout", produces = "application/json")
	public SpeechWorkout saveSpeechWorkout(@RequestParam UUID exerciseId, @RequestParam MultipartFile audioFile) throws IOException {
		Exercise exercise = exerciseRepository.findById(exerciseId).get();
		SpeechWorkout speechWorkout = new SpeechWorkout();
		speechWorkout.setUser((User)getOrCreateBaseUser());
		speechWorkout.setExercise(exercise);
		speechWorkout.setFileName(audioFile.getName());
		speechWorkout.setContentType(audioFile.getContentType());
		speechWorkout.setContent(audioFile.getBytes());
		
		return speechWorkoutRepository.save(speechWorkout);
	}

	@GetMapping(value = "/getAllSpeechWorkouts", produces = "application/json")
	Iterable<SpeechWorkout> getAllSpeechWorkouts(
			@RequestParam(required = false, defaultValue = "1") @Min(value = 1) Integer page
			, @RequestParam(required = false, defaultValue = "20") @Min(value = 1) @Max(value = 100) Integer pageSize) {
		int p = page != null ? page.intValue()-1:0;
		int ps = pageSize != null ? pageSize.intValue():20;
		return speechWorkoutRepository.findAll(PageRequest.of(p,ps)).getContent();
	}
	
	@GetMapping(value="/getSpeechWorkoutAudioFile", produces = "audio/wav")
	@ResponseBody
	public ResponseEntity<Resource> getSpeechWorkoutAudioFile(@RequestParam UUID id) {
		SpeechWorkout speechWorkout = speechWorkoutRepository.findById(id).get();
		return ResponseEntity.ok().header(
				HttpHeaders.CONTENT_DISPOSITION
				, "attachment; filename=\"" + speechWorkout.getFileName() + "\""
				).body(new ByteArrayResource(speechWorkout.getContent()));
	}
	
	@RequestMapping(value="/setUserName", method = RequestMethod.PUT, produces = "application/json")
	@ApiOperation("Sets a userName for a given user-id or scientist-id")
	public BaseUser setUserName(@RequestParam String userName) {
		BaseUser user = getOrCreateBaseUser();
		user.setUserName(userName);
		if(user.isScientist())
			return scientistRepository.save((Scientist)user);
		else
			return userRepository.save((User)user);
	}
	
	public BaseUser getOrCreateBaseUser(OidProvider oidProvider, String oid) {
		if(oidProvider.equals(OidProvider.orcid))
			return getOrCreateScientist(oidProvider, oid);
		return getOrCreateUser(oidProvider, oid);
	}
	
	private Scientist getOrCreateScientist(OidProvider oidProvider, String oid) {
		if(scientistRepository.existsByOidProviderAndOid(oidProvider, oid))
			return scientistRepository.findByOidProviderAndOid(oidProvider, oid);
		return scientistRepository.save(new Scientist(oidProvider, oid));
		
	}

	private User getOrCreateUser(OidProvider oidProvider, String oid) {
		if(userRepository.existsByOidProviderAndOid(oidProvider, oid))
			return userRepository.findUserByOidProviderAndOid(oidProvider, oid);
		return userRepository.save(new User(oidProvider, oid));
	}
	
}